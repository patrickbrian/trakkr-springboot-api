package trakkr.serviceImpl

import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import trakkr.entities.User
import trakkr.repositories.UserRepository
import trakkr.service.UserService

@Service
class UserServiceImpl(
    private val repository: UserRepository
) : UserService {
    /**
     * Create a user
     */
    override fun createUser(user: User): User {
        // save the user using the userRepository
        return repository.save(user)
    }

    /**
     * Get a specific user using an id
     */
    override fun getById(id: Long): User {
        // Find a user in the db and if it does not exist, throw a 404 error
        val user = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "User with id: $id does not exist.")
        }
        return user
    }

    override fun updateUser(body: User, id: Long): User {
        // 1. Find the entity first
        val user = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "User with id: $id does not exist.")
        }

        // todo(Ensure data integrity)

        // 2. Use save(). If the object does not exist yet in the db,
        // it will insert the object
        // else, it will update the object
        return repository.save(user.copy(
            firstName = body.firstName,
            lastName = body.lastName,
            email = body.email
        ))

    }

    override fun deleteUser(id: Long) {
        //1. Find the entity
        val user = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "User with id: $id does not exist.")
        }

        repository.delete(user)
    }

    override fun getAllUsers(): List<User> {

        // findAll returns a collection
        val users = repository.findAll()
        return users.toList()

    }
}